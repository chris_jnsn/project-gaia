﻿Shader "Custom/FinalConstruction" {
	Properties{
	_OuterTexture("OuterLayer", 2D) = "white" {}
	_InnerTexture("InnerLayer", 2D) = "white" {}
	_MaskTex("Base (RGB)", 2D) = "mask" {}
	}

		SubShader
	{
		Pass
	{
		ZTest Always Cull Off ZWrite Off

		CGPROGRAM
		#pragma vertex vert_img
		#pragma fragment frag
		#include "UnityCG.cginc"

	sampler2D _OuterTexture;
	sampler2D _InnerTexture;

	uniform sampler2D _MaskTex;

	float _pos_x;
	float _pos_y;

	float _size_innerLayer;

	fixed4 frag(v2f_img i) : SV_Target
	{
		fixed4 c1, output;

		float2 offset_Inner = i.uv;

		c1 = tex2D(_OuterTexture, i.uv);
		output = c1;

		if (i.uv.x >= (_pos_x - (_size_innerLayer / 2)) && i.uv.y >= (_pos_y - (_size_innerLayer / 2)) && i.uv.x < (_pos_x + (_size_innerLayer / 2)) && i.uv.y < (_pos_y + (_size_innerLayer / 2)))
		{
			fixed4 c2, c3;

			offset_Inner.x -= _pos_x - (_size_innerLayer / 2);
			offset_Inner.y -= _pos_y - (_size_innerLayer / 2);
			fixed4 mask_Inner = tex2D(_MaskTex, offset_Inner * (1 / _size_innerLayer));

			c2 = tex2D(_InnerTexture, offset_Inner * (1 / _size_innerLayer));
			c3 = lerp(c1, c2, mask_Inner.a);

			output = c3;
		}
	return output;
	}
		ENDCG

	}
	}

		Fallback off
}

