﻿Shader "Custom/OuterTexture" {
	Properties{
		_OuterTexture("OuterLayer", 2D) = "white" {}
	}

		SubShader
		{
			Pass
		{
			ZTest Always Cull Off ZWrite Off

			CGPROGRAM
			#pragma vertex vert_img
			#pragma fragment frag
			#include "UnityCG.cginc"

			sampler2D _OuterTexture;

			fixed4 frag(v2f_img i) : SV_Target
			{				
				fixed4 c1, output;

				float2 offset_Inner = i.uv;

				c1 = tex2D(_OuterTexture, i.uv);
				output = c1;				

				return output;
			}
				ENDCG

			}
		}

			Fallback off
}

