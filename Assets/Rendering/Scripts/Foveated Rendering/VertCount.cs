﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UnityEngine;
using Valve.VR;

public class VertCount : MonoBehaviour
{
    public static int verts;
    public static int tris;

    StreamWriter outStream;

    private List<string[]> rowData = new List<string[]>();

    string[] rowDataTemp = new string[5];

    private bool outStreamCreated;

    float deltaTime = 0.0f;

    // Use this for initialization
    void Start()
    {
        createOutstream();
        verts = 0;
        tris = 0;
        GameObject[] ob = FindObjectsOfType(typeof(GameObject)) as GameObject[];
        foreach (GameObject obj in ob)
        {
            GetObjectStats(obj);
        }
    }


    // Update is called once per frame
    void Update()
    {
        deltaTime += (Time.unscaledDeltaTime - deltaTime) * 0.1f;
        float fps = 1.0f / deltaTime;

        var vr = SteamVR.instance;
        var timing = new Compositor_FrameTiming();
        if (vr != null)
        {
            timing.m_nSize = (uint)System.Runtime.InteropServices.Marshal.SizeOf(typeof(Compositor_FrameTiming));
            vr.compositor.GetFrameTiming(ref timing, 0);
        }

        DateTime _newTimeStamp = System.DateTime.Now;

        rowDataTemp = new string[5];
        rowDataTemp[0] = _newTimeStamp.ToString();
        rowDataTemp[1] = verts.ToString();
        rowDataTemp[2] = tris.ToString();
        rowDataTemp[3] = fps.ToString();
        rowDataTemp[4] = timing.m_flTotalRenderGpuMs.ToString();
        rowData.Add(rowDataTemp);

        writeData();

    }

    void GetObjectStats(GameObject obj)
    {
        Component[] filters;
        filters = obj.GetComponentsInChildren<MeshFilter>();
        foreach (MeshFilter f in filters)
        {
            tris += f.sharedMesh.triangles.Length / 3;
            verts += f.sharedMesh.vertexCount;
        }
    }

    private void createOutstream()
    {
        string filePath = getPath();

        outStream = System.IO.File.CreateText(filePath);

        rowDataTemp[0] = "Timestamp";
        rowDataTemp[1] = "Vertex Count";
        rowDataTemp[2] = "Triangle Count";
        rowDataTemp[3] = "FPS";
        rowDataTemp[4] = "Rendertime";

        rowData.Add(rowDataTemp);

        outStreamCreated = true;
    }

    private string getPath()
    {
        String path = "D:/PerformanceLog.csv";
        return path;
    }

    private void writeData()
    {
        string[][] output = new string[rowData.Count][];

        for (int i = 0; i < output.Length; i++)
        {
            output[i] = rowData[i];
        }

        int length = output.GetLength(0);
        string delimiter = ",";

        StringBuilder sb = new StringBuilder();

        for (int index = 0; index < length; index++)
            sb.AppendLine(string.Join(delimiter, output[index]));

        outStream.WriteLine(sb);
        outStream.Flush();
        rowData.Clear();
    }

    private void OnApplicationQuit()
    {
        if (outStreamCreated)
        {
            writeData();
            outStream.Close();
        }
    }
}
