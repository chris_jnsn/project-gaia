﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;

public class AdaptiveQualityRight : MonoBehaviour
{

    private int frameCounter;
    private bool qualityChanged;
    private int currentQualityLevel;

    public FoveatedScript innerLayer;
    public FoveatedScript outerLayer;
    public FinalPassRight finalPass;

    private float sceneWidth;
    private float sceneHeight;

    private List<float> frameTimes;

    private float prediction;

    // Use this for initialization
    void Start()
    {
        frameCounter = 0;
        currentQualityLevel = 0;
        qualityChanged = true;
        prediction = 0;

        sceneWidth = 1512;
        sceneHeight = 1680;
        frameTimes = new List<float>();
    }

    // Update is called once per frame
    void Update()
    {
        var vr = SteamVR.instance;
        if (vr != null)
        {
            var timing = new Compositor_FrameTiming();
            timing.m_nSize = (uint)System.Runtime.InteropServices.Marshal.SizeOf(typeof(Compositor_FrameTiming));
            vr.compositor.GetFrameTiming(ref timing, 0);

            addElement(timing.m_flTotalRenderGpuMs);

            if (qualityChanged == true && frameCounter < 2)
            {
                frameCounter++;
            }
            else
            {
                qualityChanged = false;
                frameCounter = 0;

                if (frameTimes[2] > 10.0f)
                {
                    if (currentQualityLevel > -2)
                    {
                        adaptQuality(currentQualityLevel - 2);
                        currentQualityLevel -= 2;
                        qualityChanged = true;
                    }
                    else if (currentQualityLevel > -3)
                    {
                        adaptQuality(currentQualityLevel - 1);
                        currentQualityLevel -= 1;
                        qualityChanged = true;
                    }
                }
                else if (frameTimes[0] < 7.8f && frameTimes[1] < 7.8f && frameTimes[2] < 7.8f)
                {
                    if (currentQualityLevel < 6)
                    {
                        adaptQuality(currentQualityLevel + 1);
                        currentQualityLevel += 1;
                        qualityChanged = true;
                    }
                }
                else
                {
                    prediction = Mathf.Abs(frameTimes[0] - frameTimes[1]);
                    if (frameTimes[2] < 9.4f)
                    {
                        if (frameTimes[2] + prediction < 10.0f)
                        {
                            if (currentQualityLevel > -2)
                            {
                                adaptQuality(currentQualityLevel - 2);
                                currentQualityLevel -= 2;
                                qualityChanged = true;
                            }
                            else if (currentQualityLevel > -3)
                            {
                                adaptQuality(currentQualityLevel - 1);
                                currentQualityLevel -= 1;
                                qualityChanged = true;
                            }
                        }
                    }
                }
            }
        }
    }

    void addElement(float element)
    {
        if (frameTimes.Count < 3)
            frameTimes.Add(element);
        else
        {
            frameTimes.RemoveAt(0);
            frameTimes.Add(element);
        }
    }

    void adaptQuality(int level)
    {
        switch (level)
        {
            case 6:
                innerLayer.activeRenderTexture = 8;
                outerLayer.activeRenderTexture = 8;
                finalPass.skipFrame = false;
                break;
            case 5:
                innerLayer.activeRenderTexture = 7;
                outerLayer.activeRenderTexture = 7;
                finalPass.skipFrame = false;
                break;
            case 4:
                innerLayer.activeRenderTexture = 6;
                outerLayer.activeRenderTexture = 6;
                finalPass.skipFrame = false;
                break;
            case 3:
                innerLayer.activeRenderTexture = 5;
                outerLayer.activeRenderTexture = 5;
                finalPass.skipFrame = false;
                break;
            case 2:
                innerLayer.activeRenderTexture = 4;
                outerLayer.activeRenderTexture = 4;
                finalPass.skipFrame = false;
                break;
            case 1:
                innerLayer.activeRenderTexture = 3;
                outerLayer.activeRenderTexture = 3;
                finalPass.skipFrame = false;
                break;
            case 0:
                innerLayer.activeRenderTexture = 2;
                outerLayer.activeRenderTexture = 2;
                finalPass.skipFrame = false;
                break;
            case -1:
                innerLayer.activeRenderTexture = 1;
                outerLayer.activeRenderTexture = 1;
                finalPass.skipFrame = false;
                break;
            case -2:
                innerLayer.activeRenderTexture = 0;
                outerLayer.activeRenderTexture = 0;
                finalPass.skipFrame = false;
                break;
            case -3:
                innerLayer.activeRenderTexture = 0;
                outerLayer.activeRenderTexture = 0;
                finalPass.skipFrame = true;
                break;
            default:
                break;
        }
    }
}
