﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FinalPassLeft : MonoBehaviour
{
    public Material material;

    public PointOfRegard por;

    public Texture mask;

    public FoveatedScript outerLayer;
    public FoveatedScript innerLayer;

    [HideInInspector]
    public int width;

    [HideInInspector]
    public int height;

    [HideInInspector]
    public bool skipFrame = false;

    private int skipCounter = 0;

    private void OnPreRender()
    {


    }

    private void LateUpdate()
    {
        int sceneWidth = 1512;
        int sceneHeight = 1680;

        width = sceneWidth;
        height = sceneHeight;

        if(skipFrame == true && skipCounter < 1)
        {
            outerLayer.RenderLeft((float)por.screenPos.x, (float)por.screenPos.y);
        }
        else if(skipFrame == true && skipCounter > 0)
        {
            skipCounter = 0;
        }
        else
        {
            outerLayer.RenderLeft((float)por.screenPos.x, (float)por.screenPos.y);
        }
      
        material.SetTexture("_OuterTexture", outerLayer.getRenderTextureLeft());

        innerLayer.RenderLeft((float)por.screenPos.x, (float)por.screenPos.y);
    }

    private void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
        Graphics.Blit(source, destination, material);
    }
}
