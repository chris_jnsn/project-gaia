﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SMI;

public class PointOfRegard : MonoBehaviour
{

    private Camera cam;

    [HideInInspector]
    public Vector3 screenPos;

    // Use this for initialization
    void Start()
    {
        cam = GetComponent<Camera>();
    }

    private void FixedUpdate()
    {
        RaycastHit hitInformation;

        //Get raycast from gaze
        SMIEyeTrackingUnity.Instance.smi_GetRaycastHitFromGaze(out hitInformation);
        if (hitInformation.collider != null)
        {
            screenPos = cam.WorldToScreenPoint(hitInformation.point);
        }
        else
        {
            //If the raycast does not collide with any object, put it far away.
            float distance = 100;
            Ray gazeRay = SMI.SMIEyeTrackingUnity.Instance.smi_GetRayFromGaze();

            screenPos = cam.WorldToScreenPoint(gazeRay.origin + Vector3.Normalize(gazeRay.direction) * distance);
        }

        screenPos.x = (float)756;
        screenPos.y = (float)840;
    }
}
