﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamChanger : MonoBehaviour
{
    public GameObject fovCamLeft;
    public GameObject fovCamRight;
    public GameObject defaultCam;

    // Use this for initialization
    void Start()
    {
        fovCamLeft.SetActive(false);
        fovCamRight.SetActive(false);
        defaultCam.SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.G))
        {

            fovCamLeft.SetActive(false);
            fovCamRight.SetActive(false);
            defaultCam.SetActive(true);

        }
        if (Input.GetKey(KeyCode.H))
        {
            fovCamLeft.SetActive(true);
            fovCamRight.SetActive(true);
            defaultCam.SetActive(false);
        }
    }
}
