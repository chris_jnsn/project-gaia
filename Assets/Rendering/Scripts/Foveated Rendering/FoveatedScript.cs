﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;
using SMI;

public class FoveatedScript : MonoBehaviour
{

    public HelperScript helper;

    public bool outerLayer;
    public bool innerLayer;

    public float resolution = 1f;
    public float regionSize = 1f;

    Camera cam;

    bool fovSet = false;

    [HideInInspector]
    public RenderTexture leftEyeTexture0, leftEyeTexture1, leftEyeTexture2, leftEyeTexture3, 
        leftEyeTexture4, leftEyeTexture5, leftEyeTexture6, leftEyeTexture7, leftEyeTexture8, 
        rightEyeTexture0, rightEyeTexture1, rightEyeTexture2, rightEyeTexture3,
        rightEyeTexture4, rightEyeTexture5, rightEyeTexture6, rightEyeTexture7, rightEyeTexture8;

    int sceneWidth;
    int sceneHeight;

    [HideInInspector]
    public int activeRenderTexture;

    [HideInInspector]
    public CVRSystem hmd { get; private set; }

    [HideInInspector]
    public float l_left = 0.0f, l_right = 0.0f, l_top = 0.0f, l_bottom = 0.0f, r_left = 0.0f, r_right = 0.0f, r_top = 0.0f, r_bottom = 0.0f;

    [HideInInspector]
    public float offsetX_Inner_Left, offsetY_Inner_Left, offsetX_Inner_Right, offsetY_Inner_Right;

    [HideInInspector]
    public int curEye = 0;

    [HideInInspector]
    public Vector2 focusPoint;

    private Vector2 perpectiveOffset;

    void Start()
    {
        if (IsEditing())
            return;

        hmd = OpenVR.System;

        cam = GetComponent<Camera>();

        activeRenderTexture = 2;

        sceneWidth = 1512;
        sceneHeight = 1680;

        createRenderTextures(sceneWidth, sceneHeight);

        cam.enabled = false;

        hmd.GetProjectionRaw(EVREye.Eye_Left, ref l_left, ref l_right, ref l_top, ref l_bottom);
        hmd.GetProjectionRaw(EVREye.Eye_Right, ref r_left, ref r_right, ref r_top, ref r_bottom);

        if (innerLayer)
        {
            offsetX_Inner_Left = (l_left - l_left * regionSize) + (l_right - l_right * regionSize);
            offsetX_Inner_Left *= 0.5f;
            offsetY_Inner_Left = (l_bottom - l_bottom * regionSize) + (l_top - l_top * regionSize);
            offsetY_Inner_Left *= 0.5f;
            offsetX_Inner_Right = (r_left - r_left * regionSize) + (r_right - r_right * regionSize);
            offsetX_Inner_Right *= 0.5f;
            offsetY_Inner_Right = (r_bottom - r_bottom * regionSize) + (r_top - r_top * regionSize);
            offsetY_Inner_Right *= 0.5f;
        }
    }

    void Update()
    {

    }

    public void RenderLeft(float porX, float porY)
    {
        curEye = 0;

        cam.transform.localPosition = SteamVR.instance.eyes[curEye].pos;
        cam.transform.localRotation = SteamVR.instance.eyes[curEye].rot;

        focusPoint.x = porX / sceneWidth;
        focusPoint.y = porY / sceneHeight;

        perpectiveOffset.x = (0.5f - focusPoint.x) * helper.helperValX;
        perpectiveOffset.y = (0.5f - focusPoint.y) * helper.helperValY;

        if (outerLayer)
            ComposeProjection(l_left, l_right, l_top, l_bottom, cam.nearClipPlane, cam.farClipPlane);
        else if (innerLayer)
            ComposeProjection(l_left * regionSize + offsetX_Inner_Left - perpectiveOffset.x, l_right * regionSize + offsetX_Inner_Left - perpectiveOffset.x, l_top * regionSize + offsetY_Inner_Left - perpectiveOffset.y, l_bottom * regionSize + offsetY_Inner_Left - perpectiveOffset.y, cam.nearClipPlane, cam.farClipPlane);

        if (!fovSet)
        {
            Matrix4x4 mat = cam.projectionMatrix;
            float t = mat.m11;
            const float Rad2Deg = 180 / Mathf.PI;
            float fov = (float)(Mathf.Atan(1.0f / t) * 2.0 * Rad2Deg);
            cam.fieldOfView = fov;

            if (outerLayer)
            {
                helper.frustumHeight = Mathf.Tan(fov / 2f * Mathf.Deg2Rad);

                helper.helperValY = helper.frustumHeight * 2;
                helper.helperValX = helper.helperValY * (float)(sceneWidth) / (float)(sceneHeight);
            }

            fovSet = true;
        }

        cam.targetTexture = getRenderTextureLeft();
        cam.Render();
    }

    public void RenderRight(float porX, float porY)
    {
        curEye = 1;

        cam.transform.localPosition = SteamVR.instance.eyes[curEye].pos;
        cam.transform.localRotation = SteamVR.instance.eyes[curEye].rot;

        focusPoint.x = porX / sceneWidth;
        focusPoint.y = porY / sceneHeight;

        perpectiveOffset.x = (0.5f - focusPoint.x) * helper.helperValX;
        perpectiveOffset.y = (0.5f - focusPoint.y) * helper.helperValY;

        if (outerLayer)
            ComposeProjection(r_left, r_right, r_top, r_bottom, cam.nearClipPlane, cam.farClipPlane);
        else if (innerLayer)
            ComposeProjection(r_left * regionSize + offsetX_Inner_Right - perpectiveOffset.x, r_right * regionSize + offsetX_Inner_Right - perpectiveOffset.x, r_top * regionSize + offsetY_Inner_Right - perpectiveOffset.y, r_bottom * regionSize + offsetY_Inner_Right - perpectiveOffset.y, cam.nearClipPlane, cam.farClipPlane);

        if (!fovSet)
        {
            Matrix4x4 mat = cam.projectionMatrix;
            float t = mat.m11;
            const float Rad2Deg = 180 / Mathf.PI;
            float fov = (float)(Mathf.Atan(1.0f / t) * 2.0 * Rad2Deg);
            cam.fieldOfView = fov;

            if (outerLayer)
            {
                helper.frustumHeight = Mathf.Tan(fov / 2f * Mathf.Deg2Rad);

                helper.helperValY = helper.frustumHeight * 2;
                helper.helperValX = helper.helperValY * (float)(sceneWidth) / (float)(sceneHeight);
            }

            fovSet = true;
        }

        cam.targetTexture = getRenderTextureRight();
        cam.Render();
    }

    public RenderTexture getRenderTextureLeft()
    {
        if (activeRenderTexture == 0)
            return leftEyeTexture0;
        else if (activeRenderTexture == 1)
            return leftEyeTexture1;
        else if (activeRenderTexture == 2)
            return leftEyeTexture2;
        else if (activeRenderTexture == 3)
            return leftEyeTexture3;
        else if (activeRenderTexture == 4)
            return leftEyeTexture4;
        else if (activeRenderTexture == 5)
            return leftEyeTexture5;
        else if (activeRenderTexture == 6)
            return leftEyeTexture6;
        else if (activeRenderTexture == 7)
            return leftEyeTexture7;
        else
            return leftEyeTexture8;
    }

    public RenderTexture getRenderTextureRight()
    {
        if (activeRenderTexture == 0)
            return rightEyeTexture0;
        else if (activeRenderTexture == 1)
            return rightEyeTexture1;
        else if (activeRenderTexture == 2)
            return rightEyeTexture2;
        else if (activeRenderTexture == 3)
            return rightEyeTexture3;
        else if (activeRenderTexture == 4)
            return rightEyeTexture4;
        else if (activeRenderTexture == 5)
            return rightEyeTexture5;
        else if (activeRenderTexture == 6)
            return rightEyeTexture6;
        else if (activeRenderTexture == 7)
            return rightEyeTexture7;
        else
            return rightEyeTexture8;
    }

    public void createRenderTextures(int width, int height)
    {
        int depth = 16;
        int w = (int)(resolution * regionSize * width);
        int h = (int)(resolution * regionSize * height);

        leftEyeTexture0 = new RenderTexture((int)(w * 0.81f), (int)(h * 0.81f), depth);
        leftEyeTexture0.antiAliasing = 2;

        rightEyeTexture0 = new RenderTexture((int)(w * 0.81f), (int)(h * 0.81f), depth);
        rightEyeTexture0.antiAliasing = 2;

        leftEyeTexture1 = new RenderTexture((int)(w * 0.9f), (int)(h * 0.9f), depth);
        leftEyeTexture1.antiAliasing = 2;

        rightEyeTexture1 = new RenderTexture((int)(w * 0.9f), (int)(h * 0.9f), depth);
        rightEyeTexture1.antiAliasing = 2;

        leftEyeTexture2 = new RenderTexture((int)(w), (int)(h), depth);
        leftEyeTexture2.antiAliasing = 4;

        rightEyeTexture2 = new RenderTexture((int)(w), (int)(h), depth);
        rightEyeTexture2.antiAliasing = 4;

        leftEyeTexture3 = new RenderTexture((int)(w * 1.1f), (int)(h * 1.1f), depth);
        leftEyeTexture3.antiAliasing = 4;

        rightEyeTexture3 = new RenderTexture((int)(w * 1.1f), (int)(h * 1.1f), depth);
        rightEyeTexture3.antiAliasing = 4;

        leftEyeTexture4 = new RenderTexture((int)(w), (int)(h), depth);
        leftEyeTexture4.antiAliasing = 8;

        rightEyeTexture4 = new RenderTexture((int)(w), (int)(h), depth);
        rightEyeTexture4.antiAliasing = 8;

        leftEyeTexture5 = new RenderTexture((int)(w * 1.1f), (int)(h * 1.1f), depth);
        leftEyeTexture5.antiAliasing = 8;

        rightEyeTexture5 = new RenderTexture((int)(w * 1.1f), (int)(h * 1.1f), depth);
        rightEyeTexture5.antiAliasing = 8;

        leftEyeTexture6 = new RenderTexture((int)(w * 1.2f), (int)(h * 1.2f), depth);
        leftEyeTexture6.antiAliasing = 8;

        rightEyeTexture6 = new RenderTexture((int)(w * 1.2f), (int)(h * 1.2f), depth);
        rightEyeTexture6.antiAliasing = 8;

        leftEyeTexture7 = new RenderTexture((int)(w * 1.3f), (int)(h * 1.3f), depth);
        leftEyeTexture7.antiAliasing = 8;

        rightEyeTexture7 = new RenderTexture((int)(w * 1.3f), (int)(h * 1.3f), depth);
        rightEyeTexture7.antiAliasing = 8;

        leftEyeTexture8 = new RenderTexture((int)(w * 1.4f), (int)(h * 1.4f), depth);
        leftEyeTexture8.antiAliasing = 8;

        rightEyeTexture8 = new RenderTexture((int)(w * 1.4f), (int)(h * 1.4f), depth);
        rightEyeTexture8.antiAliasing = 8;
    }

    private bool IsEditing()
    {
        return Application.isEditor && !Application.isPlaying;
    }

    private void ComposeProjection(float fLeft, float fRight, float fTop, float fBottom, float zNear, float zFar)
    {
        var m = Matrix4x4.identity;

        float idx = 1.0f / (fRight - fLeft);
        float idy = 1.0f / (fBottom - fTop);
        float idz = 1.0f / (zFar - zNear);
        float sx = fRight + fLeft;
        float sy = fBottom + fTop;

        m[0, 0] = 2 * idx; m[0, 1] = 0; m[0, 2] = sx * idx; m[0, 3] = 0;
        m[1, 0] = 0; m[1, 1] = 2 * idy; m[1, 2] = sy * idy; m[1, 3] = 0;
        m[2, 0] = 0; m[2, 1] = 0; m[2, 2] = -zFar * idz; m[2, 3] = -zFar * zNear * idz;
        m[3, 0] = 0; m[3, 1] = 0; m[3, 2] = -1.0f; m[3, 3] = 0;

        cam.projectionMatrix = m;
    }
}
