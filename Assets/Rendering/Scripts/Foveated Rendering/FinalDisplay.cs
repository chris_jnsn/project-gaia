﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FinalDisplay : MonoBehaviour {

    [HideInInspector]
    public RenderTexture rendTex;

    // Use this for initialization
    void Start()
    {
        int width = (int)SteamVR.instance.sceneWidth;
        int height = (int)SteamVR.instance.sceneHeight;
        rendTex = new RenderTexture(width, height, 16, RenderTextureFormat.ARGB32);
    }
}
