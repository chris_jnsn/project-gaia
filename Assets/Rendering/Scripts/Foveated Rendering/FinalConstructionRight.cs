﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FinalConstructionRight : MonoBehaviour {

    public Material material;
    public FoveatedScript innerLayer;
    public FinalPassRight final;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
        setShader();
        material.SetTexture("_OuterTexture", source);
        Graphics.Blit(source, destination, material);
    }

        void setShader()
    {
        int sceneWidth = final.width;
        int sceneHeight = final.height;

        material.SetTexture("_InnerTexture", innerLayer.getRenderTextureRight());
        material.SetFloat("_pos_x", (float)final.por.screenPos.x / (float)sceneWidth);
        material.SetFloat("_pos_y", (float)final.por.screenPos.y / (float)sceneHeight);
        material.SetFloat("_size_innerLayer", innerLayer.regionSize);
        material.SetTexture("_MaskTex", final.mask);
    }
}
