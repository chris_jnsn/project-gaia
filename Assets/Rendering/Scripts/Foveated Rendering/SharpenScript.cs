﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SharpenScript : MonoBehaviour {

    public Material material;

    [Range(0.0f, 3.0f)] public float sharpenValue = 0.3f;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
        material.SetFloat("_SharpenParameters", sharpenValue);
        Graphics.Blit(source, destination, material);
    }
}


