﻿Shader "Custom/SharpenShader" {
	Properties{
		_MainTex("Base (RGB)", 2D) = "white" {}
	}

		SubShader
	{
		Pass
	{
		ZTest Always Cull Off ZWrite Off

		CGPROGRAM
#pragma vertex vert_img
#pragma fragment frag
#include "UnityCG.cginc"


		uniform sampler2D _MainTex;
	uniform float4 _MainTex_TexelSize;

	float _SharpenParameters;

	float4 frag(v2f_img i) : SV_Target
	{
		const float2 k = _MainTex_TexelSize.xy;
	float2 uv = i.uv;

	float4 color = tex2D(_MainTex, uv);

	float4 topLeft = tex2D(_MainTex, uv - k * 0.5);
	float4 bottomRight = tex2D(_MainTex, uv + k * 0.5);

	float4 corners = 4.0 * (topLeft + bottomRight) - 2.0 * color;

	// Sharpen output
	color += (color - (corners * 0.166667)) * 2.718282 * _SharpenParameters;
	color = max(0.0, color);

	// done
	return color;
	}
		ENDCG

	}
	}

		Fallback off
}
