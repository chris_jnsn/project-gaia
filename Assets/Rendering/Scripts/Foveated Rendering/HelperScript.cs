﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HelperScript : MonoBehaviour {

    [HideInInspector]
    public float helperValX, helperValY, frustumHeight;

}
