To use the foveated rendering technique you need to import the content of the Assets folder into the Assets folder of your project. Then deactivate existing cameras in your scene and integrate the following prefabs from the Rendering/Ressources folder into your scene:

- Foveated Camera Left
- Foveated Camera Right
- SMI Holder
